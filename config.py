import os
from ast import literal_eval


class Config(object):
	DEBUG = False
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	SECRET_KEY = os.environ['SECRET_KEY']
	SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']

	@classmethod
	def init_app(cls, app):
		pass


class Development(Config):
	DEBUG = True
	JWT_SECRET_KEY = os.urandom(3)


	def __repr__(self):
		return '{}'.format(self.__class__.__name__)



class Testing(Config):
	"""TODO"""
	pass


class Staging(Config):
	"""TODO"""
	SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)


class Production(Config):
	SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)


config = {
	'Development': Development,
	'Testing': Testing,
	'Production': Production,
	'Staging': Staging,

	'default': Development,


}
