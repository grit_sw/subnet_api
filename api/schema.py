from collections import namedtuple

from marshmallow import Schema, ValidationError, fields, post_load, validates, validates_schema

from api.models import CircuitTypeModel, SubnetTypeModel, CurrentTypeModel
from logger import logger


SubnetParams = namedtuple('SubnetParams', [
	'name',
	])

RMU = namedtuple('RMU', [
	'name',
	'voltage_rating',
	'current_rating',
	])

Transformer = namedtuple('Transformer', [
	'name',
	'voltage_rating',
	'power_rating',
	'frequency_rating',
	'rmus',
	])

Subnet = namedtuple('Subnet', [
	'name',
	'circuit_type_id',
	'subnet_type_id',
	'current_type_id',
	'facilities',
	'transformers',
	])

PowerProfile = namedtuple('PowerProfile', [
	'produced_power',
	'consumed_power',
	'unused_power',
	])


class SubnetParamsSchema(Schema):
	name = fields.String(required=True)

	@post_load
	def create_rmu(self, data):
		return SubnetParams(**data)

	@validates('name')
	def validate_name(self, value):
		if not value:
			raise ValidationError('Ring Main Unit (RMU) name is required')


class RMUSchema(Schema):
	name = fields.String(required=True)
	voltage_rating = fields.Float(required=True)
	current_rating = fields.Float(required=True)

	@post_load
	def create_rmu(self, data):
		return RMU(**data)

	@validates('name')
	def validate_name(self, value):
		if not value:
			raise ValidationError('Ring Main Unit (RMU) name is required')

	@validates('voltage_rating')
	def validate_voltage_rating(self, value):
		try:
			float(value)
		except ValueError as e:
			logger.exception(e)
			raise ValidationError('Voltage rating is required')

	@validates('current_rating')
	def validate_current_rating(self, value):
		try:
			float(value)
		except ValueError as e:
			logger.exception(e)
			raise ValidationError('Current rating is required')

	def __repr__(self):
		return "RMUSchema <{}>".format(self.name)


class TransformerSchema(Schema):
	name = fields.String(required=True)
	voltage_rating = fields.Float(required=True)
	power_rating = fields.Float(required=True)
	frequency_rating = fields.Float(required=True)
	rmus = fields.Nested('RMUSchema', many=True, strict=True)

	@post_load
	def create_transformer(self, data):
		return Transformer(**data)

	@validates('name')
	def validate_name(self, value):
		if not value:
			raise ValidationError('A transformer name is required.')

	@validates('voltage_rating')
	def validate_voltage_rating(self, value):
		try:
			float(value)
		except ValueError as e:
			logger.exception(e)
			raise ValidationError('Input voltage rating is required.')

	@validates('power_rating')
	def validate_power_rating(self, value):
		try:
			float(value)
		except ValueError as e:
			logger.exception(e)
			raise ValidationError('Power rating is required.')

	@validates('frequency_rating')
	def validate_frequency_rating(self, value):
		try:
			float(value)
		except ValueError as e:
			logger.exception(e)
			raise ValidationError('Frequency rating is required.')

	def __repr__(self):
		return "TransformerSchema <{}>".format(self.name)


class SubnetSchema(Schema):
	name = fields.String(required=True)
	circuit_type_id = fields.String(required=True)
	subnet_type_id = fields.String(required=True)
	current_type_id = fields.String(required=True)
	facilities = fields.List(fields.String(), required=True)
	transformers = fields.Nested('TransformerSchema', many=True, strict=True)

	@post_load
	def create_subnet(self, data):
		return Subnet(**data)

	@validates('name')
	def validate_name(self, value):
		if not value:
			raise ValidationError('A subnet name is required.')

	@validates('circuit_type_id')
	def validate_circuit_type_id(self, value):
		if not CircuitTypeModel.get_one(circuit_type_id=value):
			raise ValidationError('Invalid circuit type.')

	@validates('subnet_type_id')
	def validate_subnet_type_id(self, value):
		if not SubnetTypeModel.get_one(subnet_type_id=value):
			raise ValidationError('Invalid subnet type.')

	@validates('current_type_id')
	def validate_current_type_id(self, value):
		if not CurrentTypeModel.get_one(current_type_id=value):
			raise ValidationError('Invalid current type.')

	@validates('facilities')
	def validate_facilities(self, value):
		if not value:
			raise ValidationError('Please select a facilitto be associated with this subnet.')

	def __repr__(self):
		return "SubnetSchema <{}>".format(self.name)


class PowerProfileSchema(Schema):
	produced_power = fields.Float(required=False)
	consumed_power = fields.Float(required=False)
	unused_power = fields.Float(required=False)

	@post_load
	def create_profile(self, data):
		return PowerProfile(**data)

	@validates_schema(pass_original=True)
	def validate_produced_power(self, _, original_data):
		produced_power = float(original_data['produced_power'])
		consumed_power = float(original_data['consumed_power'])
		unused_power = float(original_data['unused_power'])
		power_difference = produced_power - consumed_power
		if unused_power != power_difference:
			raise ValidationError(f'Power difference not equal to unused power. Expected {power_difference} got {unused_power}')
