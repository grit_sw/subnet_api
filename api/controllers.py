from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import (CircuitTypeManager, CurrentTypeManager, SubnetManager,
                         SubnetTypeManager)
from api.schema import SubnetParamsSchema, SubnetSchema, PowerProfileSchema
from logger import logger

subnet_api = Namespace('subnet_api', description='Api for managing subnets')

new_type = subnet_api.model('NewType', {
	'name': fields.String(required=True, description='The name of the rmu'),
})

new_rmu = subnet_api.model('NewRMU', {
	'name': fields.String(required=True, description='The name of the rmu'),
	'voltage_rating': fields.Float(required=True, description='The input voltage of the rmu in Volts (V)'),
	'current_rating': fields.Float(required=True, description='The current rating of the rmu in Volts (V)'),
})

new_transformer = subnet_api.model('NewTransformer', {
	# A transformer has an input and output side. Only one side of the transformer will be used for GTG
	'name': fields.String(required=True, description='The name of the transformer'),
	'voltage_rating': fields.Float(required=True, description='The voltage rating of the side of the transformer used in the subnet Volts (V)'),
	'power_rating': fields.Float(required=True, description='The power rating of the transformer in Watts'),
	'frequency_rating': fields.Float(required=True, description='The frequency rating of the transformer in Hz'),
	'rmus': fields.List(fields.Nested(new_rmu), required=True, description='A list of one or more connected RMUs'),
})

new_subnet = subnet_api.model('NewSubnet', {
	'name': fields.String(required=False, description='The name of the subnet'),
	'circuit_type_id': fields.String(required=False, description='The circuit type'),
	'subnet_type_id': fields.String(required=True, description='The subnet type'),
	'current_type_id': fields.String(required=True, description='The current type'),
	'facilities': fields.List(fields.String(), required=True, description='The facilities connected to this subnet.'),
	'transformers': fields.List(fields.Nested(new_transformer), required=True, description='A list of one or more connected transformers'),
})


power_profile = subnet_api.model('Power Profile', {
	'produced_power': fields.Float(required=False, description='The produced power on the subnet.'),
	'consumed_power': fields.Float(required=False, description='The consumed power on the subnet.'),
	'unused_power': fields.Float(required=True, description='The unused power on the subnet.'),
})


@subnet_api.route('/circuit-type/new/')
class NewCircuitType(Resource):
	"""
		Api for creating a new circuit type.
	"""

	@subnet_api.expect(new_type)
	def post(self):
		"""
			HTTP method for creating a new circuit type.
			@param: payload
			@returns: response and status code
		"""
		schema = SubnetParamsSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = CircuitTypeManager()
		resp, code = manager.create(new_payload)
		return resp, code


@subnet_api.route('/circuit-type/one/<string:circuit_type_id>/')
class OneCircuitType(Resource):
	"""
		Api for viewing a circuit type.
	"""

	def get(self, circuit_type_id):
		"""
			HTTP method for viewing a circuit type.
			@param: payload
			@returns: response and status code
		"""
		manager = CircuitTypeManager()
		resp, code = manager.get_one(circuit_type_id)
		return resp, code


@subnet_api.route('/circuit-type/all/')
class AllCircuitTypes(Resource):
	"""
		Api for viewing all circuit types.
	"""

	def get(self):
		"""
			HTTP method for viewing all circuit types.
			@param: payload
			@returns: response and status code
		"""
		manager = CircuitTypeManager()
		resp, code = manager.get_all()
		return resp, code


@subnet_api.route('/subnet-type/new/')
class NewSubnetType(Resource):
	"""
		Api for creating a new subnet type.
	"""

	@subnet_api.expect(new_type)
	def post(self):
		"""
			HTTP method for creating a new subnet type.
			@param: payload
			@returns: response and status code
		"""
		schema = SubnetParamsSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = SubnetTypeManager()
		resp, code = manager.create(new_payload)
		return resp, code


@subnet_api.route('/subnet-type/one/<string:subnet_type_id>/')
class OneSubnetType(Resource):
	"""
		Api for viewing a subnet type.
	"""

	def get(self, subnet_type_id):
		"""
			HTTP method for viewing a subnet type.
			@param: payload
			@returns: response and status code
		"""
		manager = SubnetTypeManager()
		resp, code = manager.get_one(subnet_type_id)
		return resp, code


@subnet_api.route('/subnet-type/all/')
class AllSubnetTypes(Resource):
	"""
		Api for viewing all subnet types.
	"""

	def get(self):
		"""
			HTTP method for viewing all subnet types.
			@param: payload
			@returns: response and status code
		"""
		manager = SubnetTypeManager()
		resp, code = manager.get_all()
		return resp, code


@subnet_api.route('/current-type/new/')
class NewCurrentType(Resource):
	"""
		Api for creating a new current type.
	"""

	@subnet_api.expect(new_type)
	def post(self):
		"""
			HTTP method for creating a new current type.
			@param: payload
			@returns: response and status code
		"""
		schema = SubnetParamsSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = CurrentTypeManager()
		resp, code = manager.create(new_payload)
		return resp, code


@subnet_api.route('/current-type/one/<string:current_type_id>/')
class OneCurrentType(Resource):
	"""
		Api for viewing a current type.
	"""

	def get(self, current_type_id):
		"""
			HTTP method for viewing a current type.
			@param: payload
			@returns: response and status code
		"""
		manager = CurrentTypeManager()
		resp, code = manager.get_one(current_type_id)
		return resp, code


@subnet_api.route('/current-type/all/')
class AllCurrentTypes(Resource):
	"""
		Api for viewing all current types.
	"""

	def get(self):
		"""
			HTTP method for viewing all current types.
			@param: payload
			@returns: response and status code
		"""
		manager = CurrentTypeManager()
		resp, code = manager.get_all()
		return resp, code


@subnet_api.route('/new/')
class NewSubnet(Resource):
	"""
		Api for creating a new subnet.
	"""

	@subnet_api.expect(new_subnet)
	def post(self):
		"""
			HTTP method for creating a new subnet.
			@param: payload
			@returns: response and status code
		"""
		schema = SubnetSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = SubnetManager()
		resp, code = manager.create(new_payload)
		return resp, code


@subnet_api.route('/one/<string:subnet_id>/')
class OneSubnet(Resource):
	"""
		Api for obtaining subnet information.
	"""

	def get(self, subnet_id):
		"""
			HTTP method for obtaining subnet information.
			@param: payload
			@returns: response and status code
		"""
		manager = SubnetManager()
		resp, code = manager.get_one(subnet_id)
		return resp, code


@subnet_api.route('/power-info/<string:subnet_id>/')
class OnePowerInfo(Resource):
	"""
		Api for obtaining subnet power information.
	"""

	def get(self, subnet_id):
		"""
			HTTP method for obtaining subnet power information.
			@param: payload
			@returns: response and status code
		"""
		manager = SubnetManager()
		resp, code = manager.power_info(subnet_id)
		return resp, code


@subnet_api.route('/power-history/<string:subnet_id>/')
class OnePowerHistory(Resource):
	"""
		Api for obtaining subnet power history.
	"""

	def get(self, subnet_id):
		"""
			HTTP method for obtaining subnet power history.
			@param: payload
			@returns: response and status code
		"""
		manager = SubnetManager()
		resp, code = manager.power_history(subnet_id)
		return resp, code


@subnet_api.route('/set-power-profile/<string:subnet_id>/')
class SetPowerProfile(Resource):
	"""
		Api for setting power profile of the subnet.
	"""

	@subnet_api.expect(power_profile)
	def post(self, subnet_id):
		"""
			HTTP method for setting power profile of the subnet.
			@param: payload
			@returns: response and status code
		"""
		schema = PowerProfileSchema(strict=True)
		data = request.values.to_dict()
		payload = api.payload or data
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = SubnetManager()
		new_payload['subnet_id'] = subnet_id
		resp, code = manager.set_power_profile(new_payload)
		return resp, code


@subnet_api.route('/facility-has/<string:facility_id>/')
class SubnetsInFacility(Resource):
	"""
		Api for obtaining information for all a facility belongs to.
	"""

	def get(self, facility_id):
		"""
			HTTP method for obtaining information for all a facility belongs to.
			@param: payload
			@returns: response and status code
		"""
		manager = SubnetManager()
		resp, code = manager.get_my_subnets(facility_id)
		return resp, code


@subnet_api.route('/all/')
class AllSubnets(Resource):
	"""
		Api for obtaining information for all subnets.
	"""

	def get(self):
		"""
			HTTP method for obtaining information for all subnets.
			@param: payload
			@returns: response and status code
		"""
		manager = SubnetManager()
		resp, code = manager.get_all()
		return resp, code
