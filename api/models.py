from uuid import uuid4

import arrow
from api import db

from logger import logger


class Base(db.Model):
	__abstract__ = True
	id = db.Column(db.Integer, autoincrement=True, primary_key=True)
	date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
	date_modified = db.Column(
		db.DateTime,
		default=db.func.current_timestamp(),
		onupdate=db.func.current_timestamp()
	)

	def get_data(self):
		return self.to_dict()


class CircuitTypeModel(Base):
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(128))
	circuit_type_id = db.Column(db.String(50), unique=True)
	subnets = db.relationship('SubnetModel', backref='circuit_type', cascade='all, delete, delete-orphan', lazy='dynamic')

	def __init__(self, name):
		self.name = name.title()
		self.circuit_type_id = str(uuid4())

	@staticmethod
	def create(name):
		new_circuit_type = CircuitTypeModel(name=name)
		db.session.add(new_circuit_type)
		db.session.commit()
		return new_circuit_type.to_dict()

	@staticmethod
	def get_one(circuit_type_id=None, name=None):
		if circuit_type_id:
			circuit_type = db.session.query(CircuitTypeModel).filter_by(circuit_type_id=circuit_type_id).first()
		elif name:
			name = name.title()
			circuit_type = db.session.query(CircuitTypeModel).filter_by(name=name).first()
		return circuit_type

	@staticmethod
	def get_all():
		response = db.session.query(CircuitTypeModel).all()
		circuit_types = [circuit_type.to_dict() for circuit_type in response]
		return circuit_types

	@staticmethod
	def setup():
		circuit_types = ["Ring", "Radial"]
		for circuit_type in circuit_types:
			type_ = CircuitTypeModel.get_one(name=circuit_type)
			if not type_:
				CircuitTypeModel.create(circuit_type)

	def to_dict(self):
		response = {
			'name': self.name,
			'circuit_type_id': self.circuit_type_id,
		}
		return response


class SubnetTypeModel(Base):
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(128))
	subnet_type_id = db.Column(db.String(50), unique=True)
	subnets = db.relationship('SubnetModel', backref='subnet_type', cascade='all, delete, delete-orphan', lazy='dynamic')

	def __init__(self, name):
		self.name = name.title()
		self.subnet_type_id = str(uuid4())

	@staticmethod
	def create(name):
		new_subnet_type = SubnetTypeModel(name=name)
		db.session.add(new_subnet_type)
		db.session.commit()
		return new_subnet_type.to_dict()

	@staticmethod
	def get_one(subnet_type_id=None, name=None):
		if subnet_type_id:
			subnet_type = db.session.query(SubnetTypeModel).filter_by(subnet_type_id=subnet_type_id).first()
		elif name:
			name = name.title()
			subnet_type = db.session.query(SubnetTypeModel).filter_by(name=name).first()
		if subnet_type:
			return subnet_type.to_dict()
		return subnet_type

	@staticmethod
	def get_all():
		response = db.session.query(SubnetTypeModel).all()
		subnet_types = [subnet_type.to_dict() for subnet_type in response]
		return subnet_types

	@staticmethod
	def setup():
		subnet_types = ["Distribution", "Transmission"]
		for subnet_type in subnet_types:
			type_ = SubnetTypeModel.get_one(name=subnet_type)
			if not type_:
				SubnetTypeModel.create(subnet_type)

	def to_dict(self):
		response = {
			'name': self.name,
			'subnet_type_id': self.subnet_type_id,
		}
		return response


class CurrentTypeModel(Base):
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(128))
	current_type_id = db.Column(db.String(50), unique=True)
	subnets = db.relationship('SubnetModel', backref='current_type', cascade='all, delete, delete-orphan', lazy='dynamic')

	def __init__(self, name):
		self.name = name.title()
		self.current_type_id = str(uuid4())

	@staticmethod
	def create(name):
		new_current_type = CurrentTypeModel(name=name)
		db.session.add(new_current_type)
		db.session.commit()
		return new_current_type.to_dict()

	@staticmethod
	def get_one(current_type_id=None, name=None):
		if current_type_id:
			current_type = db.session.query(CurrentTypeModel).filter_by(current_type_id=current_type_id).first()
		elif name:
			name = name.title()
			current_type = db.session.query(CurrentTypeModel).filter_by(name=name).first()
		if current_type:
			return current_type.to_dict()
		return current_type

	@staticmethod
	def get_all():
		response = db.session.query(CurrentTypeModel).all()
		current_types = [current_type.to_dict() for current_type in response]
		return current_types

	@staticmethod
	def setup():
		current_types = ["AC", "DC"]
		for current_type in current_types:
			type_ = CurrentTypeModel.get_one(name=current_type)
			if not type_:
				CurrentTypeModel.create(current_type)

	def to_dict(self):
		response = {
			'name': self.name,
			'current_type_id': self.current_type_id,
		}
		return response


class RmuModel(Base):
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(128))
	voltage_rating = db.Column(db.String(128))  # in Volts (V)
	current_rating = db.Column(db.String(128))  # in Amps (A)
	rmu_id = db.Column(db.String(50), unique=True)
	transformer_id = db.Column(db.String(50), db.ForeignKey('transformer_model.transformer_id'))
	# transformer = db.relationship('TransformerModel')

	def __init__(self, name, voltage_rating, current_rating, transformer):
		self.name = name.title()
		self.voltage_rating = voltage_rating
		self.current_rating = current_rating
		self.rmu_id = str(uuid4())
		self.transformer = transformer

	@staticmethod
	def rmu_as_dict(ordered_rmu):
		rmu_dict = ordered_rmu._asdict()
		return rmu_dict

	@staticmethod
	def create_rmu(rmu, transformer):
		rmu_dict = RmuModel.rmu_as_dict(rmu)
		rmu_dict['transformer'] = transformer
		new_rmu = RmuModel(**rmu_dict)
		db.session.add(new_rmu)
		return new_rmu

	def to_dict(self):
		response = {
			'name': self.name.title(),
			'voltage_rating': self.voltage_rating,
			'current_rating': self.current_rating,
			'rmu_id': self.rmu_id,
		}
		return response


class TransformerModel(Base):
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(128))
	voltage_rating = db.Column(db.Float)  # in Volts (V)
	power_rating = db.Column(db.Float)  # in Watts (W)
	frequency_rating = db.Column(db.Float)  # in Hz
	transformer_id = db.Column(db.String(50), unique=True)
	subnet_id = db.Column(db.String(50), db.ForeignKey('subnet_model.subnet_id'))
	rmus = db.relationship('RmuModel', backref='transformer', cascade='all, delete, delete-orphan', lazy='dynamic')

	def __init__(self, name, voltage_rating, power_rating, frequency_rating, subnet):
		self.name = name.title()
		self.voltage_rating = voltage_rating
		self.power_rating = power_rating
		self.frequency_rating = frequency_rating
		self.transformer_id = str(uuid4())
		self.subnet = subnet

	@staticmethod
	def transformer_as_dict(ordered_transformer):
		transformer_dict = ordered_transformer._asdict()
		return transformer_dict

	@staticmethod
	def create_transformer(transformer, subnet):
		transformer_dict = TransformerModel.transformer_as_dict(transformer)
		rmus = transformer_dict.get('rmus')
		if not rmus:
			rmus = []
		del transformer_dict['rmus']
		transformer_dict['subnet'] = subnet
		new_transformer = TransformerModel(**transformer_dict)
		db.session.add(new_transformer)
		for rmu in rmus:
			RmuModel.create_rmu(rmu, transformer=new_transformer)
		return new_transformer

	def to_dict(self):
		response = {
			'name': self.name.title(),
			'transformer_id': self.transformer_id,
			'voltage_rating': self.voltage_rating,
			'power_rating': self.power_rating,
			'frequency_rating': self.frequency_rating,
			'rmus': [rmu.to_dict() for rmu in self.rmus.all()],
		}
		return response


class Facilities(Base):
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	facility_id = db.Column(db.String(50), unique=True)
	subnet_id = db.Column(db.String(50), db.ForeignKey('subnet_model.subnet_id'))

	@staticmethod
	def get_one_or_create(facility_id):
		facility = Facilities.query.filter_by(facility_id=facility_id).first()
		if not facility:
			facility = Facilities(facility_id=facility_id)
			db.session.add(facility)
			db.session.commit()
			db.session.refresh(facility)
		return facility


class PowerProfile(Base):
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	produced_power = db.Column(db.Float) # in Watts
	consumed_power = db.Column(db.Float) # in Watts
	unused_power = db.Column(db.Float) # in Watts
	subnet_id = db.Column(db.String(50), db.ForeignKey('subnet_model.subnet_id'))

	def __init__(self, produced_power, consumed_power, unused_power):
		self.produced_power = produced_power
		self.consumed_power = consumed_power
		self.unused_power = unused_power

	@staticmethod
	def create(produced_power, consumed_power, unused_power):
		new_power_profile = PowerProfile(
			produced_power=produced_power,
			consumed_power=consumed_power,
			unused_power=unused_power,
		)
		db.session.add(new_power_profile)
		db.session.commit()
		db.session.refresh(new_power_profile)
		return new_power_profile

	def to_dict(self):
		resp = {
			'produced_power': self.produced_power,
			'consumed_power': self.consumed_power,
			'unused_power': self.unused_power,
			'date_measured': str(arrow.get(self.date_created, 'Africa/Lagos')),
		}
		return resp

class SubnetModel(Base):
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(50))
	subnet_id = db.Column(db.String(50), unique=True)

	circuit_type_id = db.Column(db.String(70), db.ForeignKey('circuit_type_model.circuit_type_id'))
	subnet_type_id = db.Column(db.String(70), db.ForeignKey('subnet_type_model.subnet_type_id'))
	current_type_id = db.Column(db.String(70), db.ForeignKey('current_type_model.current_type_id'))
	transformers = db.relationship('TransformerModel', backref='subnet', cascade='all, delete, delete-orphan', lazy='dynamic')
	facilities = db.relationship('Facilities', backref='subnet', cascade='all, delete, delete-orphan', lazy='dynamic')
	power_profile = db.relationship('PowerProfile', backref='subnet', cascade='all, delete, delete-orphan', lazy='dynamic')

	def __init__(self, name, circuit_type_id, subnet_type_id, current_type_id):
		self.name = name.title()
		self.circuit_type = CircuitTypeModel.query.filter_by(circuit_type_id=circuit_type_id).first()
		self.subnet_type = SubnetTypeModel.query.filter_by(subnet_type_id=subnet_type_id).first()
		self.current_type = CurrentTypeModel.query.filter_by(current_type_id=current_type_id).first()
		self.subnet_id = str(uuid4())

	@staticmethod
	def create_subnet(name, circuit_type_id, subnet_type_id, current_type_id, facilities, transformers):
		new_subnet = SubnetModel(
			name,
			circuit_type_id,
			subnet_type_id,
			current_type_id,
		)
		db.session.add(new_subnet)
		for facility_id in facilities:
			new_subnet.facilities.append(Facilities.get_one_or_create(facility_id))

		# db.session.commit()
		# db.session.refresh(new_subnet)
		for transformer in transformers:
			TransformerModel.create_transformer(transformer, subnet=new_subnet)
		db.session.commit()
		db.session.refresh(new_subnet)
		return new_subnet.to_dict()
	
	def replace_facilities(self, new_facilities):
		for facility in self.facilities:
			logger.info(self.facilities)
			self.facilities.remove(facility)
		for facility_id in new_facilities:
			self.facilities.append(Facilities.get_one_or_create(facility_id))
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	@staticmethod
	def get_one(facility_id=None, name=None, subnet_id=None):
		"""Get one subnet object using name or subnet_id"""
		if subnet_id:
			subnet = SubnetModel.query.filter_by(subnet_id=subnet_id).first()
		elif name:
			name = name.title()
			subnet = SubnetModel.query.filter_by(name=name).first()
		else:
			return None

		if facility_id:
			# Get a facility in a subnet
			if hasattr(subnet, 'facilities'):
				subnet = subnet.facilities.filter_by(facility_id=facility_id).first()
		return subnet

	@staticmethod
	def get_my_subnets(facility_id):
		# All the subnets a facility has access to.
		subnets = SubnetModel.query.join(SubnetModel.facilities).filter_by(facility_id=facility_id).all()
		subnets = [subnet.to_dict() for subnet in subnets]
		return subnets

	def set_power_profile(self, data):
		new_power_profile = PowerProfile.create(
			produced_power=data['produced_power'],
			consumed_power=data['consumed_power'],
			unused_power=data['unused_power'],
		)
		self.power_profile.append(new_power_profile)
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.power_info_dict()

	def latest_power_info(self):
		"""Latest power information for one subnet"""
		power_profile = self.power_profile.order_by(PowerProfile.id.desc()).first()
		return power_profile.to_dict()

	def subnet_power_history(self):
		"""Power information history for one subnet"""
		power_profiles = self.power_profile.order_by(PowerProfile.id.desc()).all()
		return [power_profile.to_dict() for power_profile in power_profiles]

	@staticmethod
	def all_power_info():
		"""All power information for all subnets."""
		all_subnets = SubnetModel.query.all()
		return [subnet.power_info_dict() for subnet in all_subnets]

	@staticmethod
	def get_all():
		"""All power information for all subnets."""
		all_subnets = SubnetModel.query.all()
		return [subnet.to_dict() for subnet in all_subnets]

	def power_info_dict(self):
		response = {
			'name': self.name,
			'subnet_id': self.subnet_id,
			'power_info':  self.latest_power_info(),
			**self.to_dict()
		}
		return response

	def get_data(self):
		return self.to_dict()

	def to_dict(self):
		response = {
			'name': self.name,
			# 'power_profile': self.power_info_dict(),
			'subnet_id': self.subnet_id,
			'circuit_type': self.circuit_type.to_dict(),
			'subnet_type': self.subnet_type.to_dict(),
			'current_type': self.current_type.to_dict(),
			'transformers': [transformer.to_dict() for transformer in self.transformers.all()],
			'facilities': [facility.facility_id for facility in self.facilities.all()],
		}
		return response
