# from api.models import SubnetModel
from flask import request

from api.models import SubnetModel, CircuitTypeModel, SubnetTypeModel, CurrentTypeModel
from logger import logger


class CircuitTypeManager(object):
	def create(self, payload):
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		name = payload.get('name')
		circuit_type = CircuitTypeModel.get_one(name=name)
		if circuit_type:
			response['success'] = False
			response['message'] = 'Circuit type {} already exists.'.format(name)
			return response, 409

		new_circuit_type = CircuitTypeModel.create(name)
		response['success'] = True
		response['data'] = new_circuit_type
		return response, 200

	def get_one(self, circuit_type_id):
		response = {}

		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		if role_id < 4:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		circuit_type = CircuitTypeModel.get_one(circuit_type_id=circuit_type_id)
		if not circuit_type:
			response['success'] = False
			response['message'] = 'Circuit type not found.'
			return response, 404
		response['success'] = True
		response['data'] = circuit_type
		return response, 201

	def get_all(self):
		response = {}

		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		if role_id < 4:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		circuit_type = CircuitTypeModel.get_all()
		if not circuit_type:
			response['success'] = True
			response['data'] = 'No circuit types found'
			return response, 404

		response['success'] = True
		response['data'] = circuit_type
		return response, 201


class SubnetTypeManager(object):
	def create(self, payload):
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		name = payload.get('name')
		subnet_type = SubnetTypeModel.get_one(name=name)
		if subnet_type:
			response['success'] = False
			response['message'] = 'Subnet type {} already exists.'.format(name)
			return response, 409

		new_subnet_type = SubnetTypeModel.create(name)
		response['success'] = True
		response['data'] = new_subnet_type
		return response, 200

	def get_one(self, subnet_type_id):
		response = {}

		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		if role_id < 4:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		subnet_type = SubnetTypeModel.get_one(subnet_type_id=subnet_type_id)
		if not subnet_type:
			response['success'] = False
			response['message'] = 'Subnet type not found.'
			return response, 404
		response['success'] = True
		response['data'] = subnet_type
		return response, 201

	def get_all(self):
		response = {}

		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		if role_id < 4:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		subnet_type = SubnetTypeModel.get_all()
		if not subnet_type:
			response['success'] = True
			response['data'] = 'No subnet types found'
			return response, 404

		response['success'] = True
		response['data'] = subnet_type
		return response, 201


class CurrentTypeManager(object):
	def create(self, payload):
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		name = payload.get('name')
		current_type = CurrentTypeModel.get_one(name=name)
		if current_type:
			response['success'] = False
			response['message'] = 'Current type {} already exists.'.format(name)
			return response, 409

		new_current_type = CurrentTypeModel.create(name)
		response['success'] = True
		response['data'] = new_current_type
		return response, 200

	def get_one(self, current_type_id):
		response = {}
		current_type = CurrentTypeModel.get_one(current_type_id=current_type_id)

		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		if role_id < 4:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		if not current_type:
			response['success'] = False
			response['message'] = 'Current type not found.'
			return response, 404
		response['success'] = True
		response['data'] = current_type
		return response, 200

	def get_all(self):
		response = {}

		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		if role_id < 4:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		current_type = CurrentTypeModel.get_all()
		if not current_type:
			response['success'] = True
			response['data'] = 'No current types found'
			return response, 404

		response['success'] = True
		response['data'] = current_type
		return response, 200


class SubnetManager(object):
	def create(self, subnet_payload):
		response = {}

		# name = subnet_payload.get('name')
		facilities = subnet_payload.get('facilities')

		role_id = request.cookies.get('role_id')
		creator_facilities = request.cookies.get('facilities')
		subnet_payload = dict(subnet_payload)
		transformers = subnet_payload.get('transformers')
		logger.info("subnet_payload {}".format(subnet_payload))
		logger.info("transformers {}".format(transformers))

		try:
			role_id = int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		if role_id < 5:
			if not all([role_id, creator_facilities]):
				response['success'] = False
				response['message'] = 'Unauthourised'
				return response, 403

			for facility_id in facilities:
				if facility_id not in creator_facilities:
					response['success'] = False
					response['message'] = 'Unauthourised.'
					return response, 403

		if role_id < 4:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		name = subnet_payload['name']
		if name.title() == 'Simulator Subnet':
			sim_subnet = SubnetModel.get_one(name=name)
			if sim_subnet:
				data = sim_subnet.replace_facilities(subnet_payload['facilities'])
				response['success'] = True
				response['data'] = data
				return response, 200

		new_subnet = SubnetModel.create_subnet(**subnet_payload)
		response['success'] = True
		response['data'] = new_subnet
		return response, 200

	def get_one(self, subnet_id):
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		subnet = SubnetModel.get_one(subnet_id=subnet_id)
		if not subnet:
			response['success'] = False
			response['message'] = 'Subnet not found.'
			return response, 404
		response['success'] = True
		response['data'] = subnet.get_data()
		return response, 200

	def power_info(self, subnet_id):
		response = {}

		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		if role_id < 6:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		subnet = SubnetModel.get_one(subnet_id=subnet_id)
		if not subnet:
			response['success'] = False
			response['message'] = 'Subnet not found.'
			return response, 404
		response['success'] = True
		response['data'] = subnet.latest_power_info()
		return response, 200

	def power_history(self, subnet_id):
		response = {}

		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		if role_id < 6:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		subnet = SubnetModel.get_one(subnet_id=subnet_id)
		if not subnet:
			response['success'] = False
			response['message'] = 'Subnet not found.'
			return response, 404
		response['success'] = True
		response['data'] = subnet.subnet_power_history()
		return response, 200

	def set_power_profile(self, payload):
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		if role_id < 6:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		subnet_id = payload['subnet_id']
		subnet = SubnetModel.get_one(subnet_id=subnet_id)
		if not subnet:
			response['success'] = False
			response['message'] = 'Subnet not found.'
			return response, 404
		del payload['subnet_id']
		data = subnet.set_power_profile(payload)
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_my_subnets(self, facility_id):
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		# Anyone in the facility should see the subnets the facility belongs to.
		if role_id < 5:
			if facility_id not in set(request.cookies.get('facilities')):
				response['success'] = False
				response['message'] = 'Unauthourised.'
				return response, 403

		data = SubnetModel.get_my_subnets(facility_id)
		if not data:
			response['success'] = False
			response['message'] = 'There is no subnet in this facility.'
			return response, 404

		response['success'] = True
		response['data'] = data
		return response, 200

	def get_all(self):
		response = {}

		role_id = request.cookies.get('role_id')
		try:
			role_id = int(role_id)
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403

		subnet_data = SubnetModel.get_all()
		if not subnet_data:
			response['success'] = False
			response['data'] = subnet_data
			return response, 404

		response['success'] = True
		response['data'] = subnet_data
		return response, 200
